(function ($, Drupal, CKEDITOR) {
CKEDITOR.plugins.add('drupaltaxonomytooltip', {
    icons: 'drupaltaxonomytooltip', // %REMOVE_LINE_CORE%
    init: function( editor ) {
        editor.addCommand( 'addTooltip', {
        	allowedContent: {
            p: {
              classes: {}
            },
            div: {
            	classes: {}
            },
          },
          modes: { wysiwyg: 1 },
          canUndo: true,
            exec: function( editor ) {
                var sel = editor.getSelection().getStartElement();
                var existingValues = {};
                var termSearchDialogAjaxCallback = function termSearchDialogAjaxCallback(returnValues) {
                	if (sel) {
                		if (returnValues.selected_tid) {
                			const tooltipID = 'taxonomy-tooltip-' + returnValues.selected_tid;
                			const tooltipHtml = '<div id="' + tooltipID + '" class="hidden">' + returnValues.description + '</div>';
            					editor.fire( 'lockSnapshot' );
                			sel.setAttributes( {
                  	    'class': 'js-simple-tooltip',
                  	    'data-simpletooltip-content-id': tooltipID,
                  	  }).appendHtml(tooltipHtml);
            					editor.fire( 'unlockSnapshot' );
                			
                			console.log(tooltipHtml);
                		}
                		

                  	return;
                	}
                	
                };
                var dialogSettings = {
                    title: 'Taxonomy tree',
                    dialogClass: 'editor-drupaltaxonomytooltip-dialog'
                };
                //console.log(sel);
                Drupal.ckeditor.openDialog(editor, Drupal.url('ckeditor_taxonomy_tooltip/dialog/statistical_programs,methodology/default/0/1,4'), existingValues, termSearchDialogAjaxCallback, dialogSettings);//
            }
        });
        if ( editor.ui.addButton ) {
            editor.ui.addButton( 'drupaltaxonomytooltip', {
                label: 'Taxonomy Tooltip',
                id: 'drupaltaxonomytooltip',
                command: 'addTooltip',
            } );
        }
    }
} );
})(jQuery, Drupal, CKEDITOR);