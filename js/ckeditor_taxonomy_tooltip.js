/**
 * @file
 * Entity Reference Tree JavaScript file.
 */

// Codes run both on normal page loads and when data is loaded by AJAX (or BigPipe!)
// @See https://www.drupal.org/docs/8/api/javascript-api/javascript-api-overview
(function($, Drupal) {
  Drupal.behaviors.entityReferenceTree = {
    attach: function(context, settings) {
    	$('#ckeditor-taxonomy-tree-wrapper', context).once('jstreeBehavior').each(function () {
    		var treeContainer = $(this);
        const selectedIdElement = $('#ckeditor-taxonomy-selected-tid');
    		const theme = treeContainer.attr('theme');
    		const dots = treeContainer.attr('dots');
    		// Avoid ajax callback from running following codes again. 
    		if (selectedIdElement.length) {
      		const selectedIDs = selectedIdElement.val().split(",");
      		const bundle = $('#ckeditor-taxonomy-vocabulary-id').val();
      		// Build the tree.
      		treeContainer.jstree({ 
        		'core' : {
        			'data' : {
        		    'url' : function (node) {
        		      return "/admin/entity_reference_tree/json/taxonomy_term/" + bundle;
        		    },
        		    'data' : function (node) {
        		      return { 'id' : node.id, 'text': node.text, 'parent': node.parent, };
        		    }
        			},
        			'themes': {
        				'dots': dots === '1' ? true : false,
        				"name": theme,
        			}
            },
            "checkbox" : {
              "three_state" : false
            },
            "search" : {
            	"show_only_matches": true,
            },
            "plugins" : [
              "search",
              "changed",
              "checkbox",
            ]
        	});
      		// Initialize the selected node.
      		treeContainer.on("loaded.jstree", function (e, data) { data.instance.select_node(selectedIDs); })
      		// Selected event.
      		treeContainer.on(
              "changed.jstree", function(evt, data){
                //selected node objects;
              	const selectedNodes = data.selected;
              	var r = [], id = [], selectedText, selectedID;
                for (var i = 0; i < selectedNodes.length; i++) {
                	const node = data.instance.get_node(selectedNodes[i]);
                	var nodeText = node.text + ' (' + node.id + ')';
                  r.push(nodeText);
                  id.push(node.id);
                }
                selectedText = r.join(', ');
                selectedID = id.join(',');
                selectedIdElement.val(selectedID);
                $('#ckeditor-taxonomy-tree-selected-text').text('Selected taxonomy terms: ' + selectedText);
            }
          );
        	// Search filter box.
        	 var to = false;
           $('#ckeditor-taxonomy-search').keyup(function () {
          	 var searchInput = $(this)
             if(to) {
            	 clearTimeout(to); 
             }
             to = setTimeout(
            		 function () {
                   var v = searchInput.val();
                   treeContainer.
                   jstree(true).
                   search(v);
                 },
                 250);
             });
    		}
    	});
    }
  }
})(jQuery, Drupal);