<?php

namespace Drupal\ckeditor_taxonomy_tooltip\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\ckeditor\CKEditorPluginCssInterface;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "Drupal Vocabulary List" plugin.
 *
 * @CKEditorPlugin(
 *   id = "drupaltaxonomytooltip",
 *   label = @Translation("Drupal Taxonomy Tooltip"),
 *   module = "ckeditor_taxonomy_tooltip"
 * )
 */
class TaxonomyTooltip extends CKEditorPluginBase implements CKEditorPluginCssInterface {
    /**
     * {@inheritdoc}
     */
    public function getFile() {
        return drupal_get_path('module', 'ckeditor_taxonomy_tooltip') . '/js/plugins/drupaltaxonomytooltip/plugin.js';
    }
    
    /**
     * {@inheritdoc}
     */
    public function getDependencies(Editor $editor) {
        return [];
    }
    
    /**
     * {@inheritdoc}
     */
    public function getLibraries(Editor $editor) {
        return ['ckeditor_taxonomy_tooltip/simple_tooltip'];
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCssFiles(Editor $editor) {
      return [
          drupal_get_path('module', 'ckeditor_taxonomy_tooltip') . '/css/simple-tooltip/simple-tooltip-styles.css',
      ];
    }
    
    /**
     * {@inheritdoc}
     */
    public function isInternal() {
        return FALSE;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getButtons() {
        return [
            'drupaltaxonomytooltip' => [
                'label' => t('Taxonomy Tooltip'),
                'image' => drupal_get_path('module', 'ckeditor_taxonomy_tooltip') . '/js/plugins/drupaltaxonomytooltip/icons/drupaltaxonomytooltip.png',
                'icons' => drupal_get_path('module', 'ckeditor_taxonomy_tooltip') . '/js/plugins/drupaltaxonomytooltip/icons/drupaltaxonomytooltip.png',
            ],
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public function isEnabled(Editor $editor) {
    }
    
    /**
     * {@inheritdoc}
     */
    public function getConfig(Editor $editor) {
        return [];
    }
}