<?php

namespace Drupal\ckeditor_taxonomy_tooltip\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;


/**
 * CKEditorTaxonomyController class
 * @author Mingsong Hu
 *
 */
class CKEditorTaxonomyController extends ControllerBase {
  
  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilder
   */
  protected $formBuilder;
  
  /**
   * The CKEditorTaxonomyController constructor.
   *
   * @param \Drupal\Core\Form\FormBuilder $formBuilder
   *   The form builder.
   */
  public function __construct(FormBuilder $formBuilder) {
    $this->formBuilder = $formBuilder;
  }
  
  /**
   * {@inheritdoc}
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The Drupal service container.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
        $container->get('form_builder')
        );
  }
  
  /**
   * Callback for opening the dialog form.
   */
  public function openTooltipDialog(string $bundle, string $theme, int $dots, string $selected) {
    $response = new AjaxResponse();
    
    // Get the modal form using the form builder.
    $modal_form = $this->formBuilder->getForm('Drupal\entity_reference_tree\Form\SearchForm', '', $bundle, 'taxonomy_term', $theme, $dots);
    
    // Add an AJAX command to open a modal dialog with the form as the content.
    $response->addCommand(new OpenModalDialogCommand($this->t('Taxonomy Tooltip'), $modal_form, ['width' => '900']));
    
    return $response;
  }  
}

