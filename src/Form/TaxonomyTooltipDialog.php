<?php

namespace Drupal\ckeditor_taxonomy_tooltip\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\editor\Ajax\EditorDialogSave;
use Drupal\taxonomy\Entity\Term;

class TaxonomyTooltipDialog extends FormBase
{
  
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ckeditor_taxonomy_tooltip_form';
  }
  
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, string $bundles = '', string $theme = 'default', int $dots = 0, string $selected = '') {
    // Do nothing after the form is submitted.
    if (!empty($form_state->getValues()) || empty($bundles)) {
      return [];
    }
    // The default values are set directly from \Drupal::request()->request,
    // provided by the editor plugin opening the dialog.
    $user_input = $form_state->getUserInput();
    
    // The status messages that will contain any form errors.
    $form['status_messages'] = [
        '#type' => 'status_messages',
        '#weight' => -10,
    ];
    // Hidden field for submitting selected entity IDs.
    $form['selected_tid'] = [
        '#type' => 'hidden',
        '#weight' => 90,
        '#value' => isset($user_input['selected_tid']) ? $user_input['selected_tid'] : $selected,
        '#attributes' => [
            'id' => [
                'ckeditor-taxonomy-selected-tid',
            ],
        ],
    ];
    // Search filter box.
    $form['tree_search'] = [
        '#type' => 'textfield',
        '#title' => $this
        ->t('Search'),
        '#size' => 60,
        '#attributes' => [
            'id' => [
                'ckeditor-taxonomy-search',
            ],
        ],
    ];
    // JsTree container.
    $form['tree_container'] = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#attributes' => [
            'id' => [
                'ckeditor-taxonomy-tree-wrapper',
            ],
            'theme' => $theme,
            'dots' => $dots,
        ],
    ];
    // Submit button.
    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['send'] = [
        '#type' => 'submit',
        '#value' => $this->t('Save'),
        '#attributes' => [
            'class' => [
                'use-ajax',
            ],
        ],
        '#ajax' => [
            'callback' => [$this, 'submitForm'],
            'event' => 'click',
        ],
    ];
    
    $form['#attached']['library'][] = 'entity_reference_tree/jstree';
    $form['#attached']['library'][] = 'ckeditor_taxonomy_tooltip/admin';
    $form['#attached']['library'][] = 'entity_reference_tree/jstree_' . $theme . '_theme';
    
    // Disable the cache for this form.
    $form_state->setCached(FALSE);
    $form['#cache'] = ['max-age' => 0];
    $form['#attributes']['data-user-info-from-browser'] = FALSE;
    // Vocabulary ID.
    $form['vocabulary_id'] = [
        '#name' => 'vocabulary_id',
        '#type' => 'hidden',
        '#weight' => 80,
        '#value' => $bundles,
        '#attributes' => [
            'id' => [
                'ckeditor-taxonomy-vocabulary-id',
            ],
        ],
    ];
    // Selectd term text.
    $form['selected_text'] = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#value' => $this
        ->t('Selected terms'),
        '#weight' => 1000,
        '#attributes' => [
            'class' => [
                'selected-entities-text',
            ],
            'id' => [
                'ckeditor-taxonomy-tree-selected-text',
            ],
        ],
    ];
    
    $form['#attached']['library'][] = 'editor/drupal.editor.dialog';
    
    return $form;    
  }
  
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    
    // If there are any form errors, re-display the form.
    if ($form_state->hasAnyErrors()) {
      $response->addCommand(new ReplaceCommand('#entity_reference_tree_wrapper', $form));
    }
    else {
      $values = $form_state->getValues();
      $tids = explode(',', $values['selected_tid']);
      $terms = Term::loadMultiple($tids);
      $descriptions = '';
      if (!empty($terms)) {
        foreach ($terms as $term) {
          $descriptions .= '<br>' . $term->getDescription();
        }
      }
      $values['description'] = $descriptions;
      $response->addCommand(new EditorDialogSave($values));
      $response->addCommand(new CloseModalDialogCommand());
    }
    
    return $response;
  }
}

